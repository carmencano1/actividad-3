using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    CharacterController _cmpCC;
    Camera _mainCamera;
    [Header("Set Variables")]
    public float _speed = 3.0f;
    public float _crouchSpeed = 2.0f;
    public float _runSpeed = 5.5f;
    float _currentSpeed = 3.0f;
    public float _cameraUpSpeed = 70.0f;
    public float _cameraLateralSpeed = 70.0f;
    [Space]
    public float _rotateObjectSpeed = 90.0f;
    Vector3 _movement = Vector3.zero;

    float _xRotCamera = 0;

    bool _isCrouched = false;
    float _startTimer = 0.0f;
    float _startMaxTimer = 0.6f;
    bool _playerActivated = false;


    private void Awake()
    {
        _cmpCC = GetComponent<CharacterController>();
        _mainCamera = Camera.main;

        _xRotCamera = _mainCamera.transform.eulerAngles.x;
        _currentSpeed = _speed;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Run();
        Rotate();
        Crouch();
    }

    void Movement()
    {
        float moveY = _movement.y;
        float xAxis = Input.GetAxis("Horizontal");
        float yAxis = Input.GetAxis("Vertical");
        _movement = (transform.forward * yAxis + transform.right * xAxis);
        if (_movement.magnitude > 1) _movement.Normalize();
        _movement *= _currentSpeed;

        if (!_cmpCC.isGrounded) _movement.y = moveY - 9.8f * Time.deltaTime;
        else _movement.y = 0.0f;

        _cmpCC.Move(_movement * Time.deltaTime);
    }

    void Run()
    {
        if (Input.GetKey(KeyCode.LeftShift) && !_isCrouched)
        {
            _currentSpeed = _runSpeed;
        }
        else
        {
            _currentSpeed = _speed;
        }
    }

    void Rotate()
    {
        float yMouse = Input.GetAxis("Mouse Y");
        _xRotCamera -= yMouse * _cameraUpSpeed * Time.deltaTime;
        _xRotCamera = Mathf.Clamp(_xRotCamera, -90.0f, 90.0f);
        _mainCamera.transform.eulerAngles = new Vector3(_xRotCamera, _mainCamera.transform.eulerAngles.y, _mainCamera.transform.eulerAngles.z);

        float xMouse = Input.GetAxis("Mouse X");
        transform.Rotate(Vector3.up * xMouse * _cameraLateralSpeed * Time.deltaTime);
    }

    void Crouch()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (_isCrouched)
            {
                _isCrouched = false;
                _currentSpeed = _speed;
                _cmpCC.height = 1.8f;
            }
            else
            {
                _isCrouched = true;
                _currentSpeed = _crouchSpeed;
                _cmpCC.height = 1.0f;
            }
        }
    }
}

