using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    public GameObject interactPanel;
    public Animator thisAnimator;
    public bool doOnce = false;
    bool canInteract = false;

    private void Update()
    {
        if (canInteract)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                thisAnimator.SetBool("DoAction", !thisAnimator.GetBool("DoAction"));

                if (doOnce)
                {
                    gameObject.SetActive(false);
                    interactPanel.SetActive(false);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canInteract = true;
            interactPanel.SetActive(true);
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canInteract = false;
            interactPanel.SetActive(false);
        }
    }
}
